# OverrideMod | OrangePix Srl

consente di fare override in qualsiasi cartella di prestashop senza fare danni
- backup automatico dei file originali
- rollback dei file sovrascriti

#Utilizzo
- Impostare i file desiderati al interno della cartella 'files_to_override', rispetando la struttura di cartelle relativi al path base di Prestashop
(es.) files_to_override/themes/classic/templates/page.tpl
- Nella procedura di installazione indicare la estensione .tpl