<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class OverrideMod extends Module
{
    protected $config_form = false;
    protected $html = '';
    protected $operation_logs = '';
    protected $override_folder = 'files_to_override/';

    public function __construct()
    {
        $this->name = 'overridemod';
        $this->tab = 'migration_tools';
        $this->version = '1.0.0';
        $this->author = 'OrangePix Srl';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Override Mod | OrangePix Srl');
        $this->description = $this->l('Install Custom overrides on proyect Folders and backup originals');

        $this->confirmUninstall = $this->l('This action deactivates the classes that this module has modified.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $this->installTemplatesOverrides($this->local_path.$this->override_folder,'tpl');
        Tools::clearAllCache();

        @parent::install();
        $this->registerHook('header') ;
        $this->registerHook('backOfficeHeader');

        return true;
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        $this->DeleteOverrideFiles();
        $this->rollbackTemplatesOverrides($this->local_path.$this->override_folder,'tpl');
        Tools::clearAllCache();
        @parent::uninstall();

        return true;
    }


    /**
     * Install and override files on proyect folders
     * @param string $directory
     * @param string $ext file extension (default php)
     */
    public function installTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $original_file = _PS_ROOT_DIR_.'/'.$file;
            if ($this->rename_file($original_file,$name.'.backup',false) || !file_exists($original_file)){
                PrestaShopLogger::addLog('Renamed File: '.$file.' -> to: '.$name.'.backup',1);
                copy($directory.$file,$original_file);
                PrestaShopLogger::addLog('Override File: '.$file,1);
            }

        }
    }


    /**
     * Rollback Installed files on proyect folders
     * @param string $directory
     * @param string $ext file extension (default php)
     */
    public function rollbackTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $path = pathinfo($file)['dirname'];
            $original_name = pathinfo($file)['basename'];
            $override_file = _PS_ROOT_DIR_.'/'.$file;
            $backup_file= _PS_ROOT_DIR_.'/'.$path.'/'.$name.'.backup';
            if (file_exists($backup_file)){
                unlink($override_file);
                PrestaShopLogger::addLog('Delete File '.$file,1);
                $this->rename_file($backup_file,$original_name,false);
                PrestaShopLogger::addLog('Rollback File '.$file.'.backup',1);
            }
        }
    }

    /**
     * Rename file
     * @param string    $old_path absolute path to file
     * @param string    $name new name
     * @param bool      $keep_ext Preserve original Extension or not
     *
     * @return bool
     */
    public function rename_file($old_path, $name,$keep_ext=true)
    {
        if (file_exists($old_path)) {
            $info=pathinfo($old_path);
            $new_path=$info['dirname']."/".($keep_ext ? $name.".".$info['extension'] : $name);
            if (file_exists($new_path)) {
                return false;
            }
            return rename($old_path, $new_path);
        }
        return false;
    }

    /**
     * Scan files in ThisModule/override/ Folder
     * @param $directory
     *
     * @return array
     */
    public function ScanOverrideFiles($directory){
        $files = array();
        foreach (Tools::scandir($directory, 'php', '', true) as $file) {
            $files[] = $file;
        }
        return $files;
    }


    /**
     * Delete files used by this module in rootProyect/override/ Folder
     * @return bool
     */
    public function DeleteOverrideFiles(){

        if (!parent::uninstallOverrides()){
            $directory = $this->local_path.'override/';
            $overrides = json_encode($this->ScanOverrideFiles($directory)) ;
            $overrides = str_replace('[','',$overrides);
            $overrides = str_replace(']','',$overrides);
            $overrides = explode(',',stripslashes($overrides));


            foreach ($overrides as $file){
                $file =  explode('override/',trim($file,"\"") )[1];
                if (file_exists(_PS_OVERRIDE_DIR_.$file) && !is_dir(_PS_OVERRIDE_DIR_.$file)){
                    try {
                        unlink(_PS_OVERRIDE_DIR_.$file);
                    }catch (Exception $e){
                        $this->displayError($e->getMessage());
                        return false;
                    }
                }
            }
            return true;
        }

    }

    /**
     * Load the configuration form
     *
     * @throws PrestaShopException
     */
    public function getContent()
    {
        $this->html = '';
        $this->operation_logs = '';
        /**
         * If values have been submitted in the form, process.
         */

        if (((bool)Tools::isSubmit('import')) == true) {

        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->html;
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }


    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

    }



}